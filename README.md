# Github Repo Issues Insights

## Project setup
```
npm install
```

### Compiles and hot-reloads
```
npm run serve
```

#### How to get the Insights

- Enter the public github Repository URL in the input field and then press "GO" button.
- The green table shown below will display all the fields like 
    - Total number of open issues
    - Number of open issues that were opened in the last 24 hours
    - Number of open issues that were opened more than 24 hours ago but less than 7 days ago
    - Number of open issues that were opened more than 7 days ago 

![Screenshot](/public/Screenshot.png)

### How GitHub Developers APIs have been used

- Gihub provides with the api to look at issues' details using API `/repos/:owner/:repo/issues` given here: [https://developer.github.com/v3/issues/#list-issues-for-a-repository](https://developer.github.com/v3/issues/#list-issues-for-a-repository)

- To find all the open issues parameters used `?state=open&sort=created&direction=desc`, get the first element and see its field `number`
    - `state = open` meaning all the open issues
    - `sort = created` meaning sort w.r.t. created timestamp
    - `direction = desc` meaning sort in descending order so as to get the last item first
    
<br>

- To find all the number of open issues that were opened in the last 24 hours, parameters used `?since={time}` (here time is calculated as last 24 hours from now)
    - response will have the next link as well as the last link provided in the header.
    - the total count will be <b>the total number of pages to be called before calling the last page</b> multiplied by the <b>number of data elements in the first response</b>
    - at last <b>adding the number of data elements from last page</b>

<br>

- To find the number of open issues that were opened more than 24 hours ago but less than 7 days ago parameters used `?since={time}` (here time is calculated as last 7 days)
    - total count will be calculated same as described above for 24 hours
    - then subtracting the count that was recieved from previous answer i.e. the issues from past 24 hours.

<br>

- To find the number of open issues that were opened more than 7 days ago
    - subtract the <b>issues count corresponding to 7 days ago</b> from <b>total number of open issues previously derived</b>